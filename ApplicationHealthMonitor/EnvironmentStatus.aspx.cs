﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ServiceProcess;

namespace ApplicationHealthMonitor
{
    public partial class EnvironmentStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PickWcisServices();
            }
        }
        protected void PickWcisServices()
        {
            ServiceList _ServiceList = new ServiceList();
            List<string> _Services = new List<string> { "W3SVC", "XblAuthManager" };
            List<ServiceList> serviceList = new List<ServiceList>();
            ServiceController[] services = ServiceController.GetServices();
            foreach (var x in services)
            {
                if (_Services.Contains(x.ServiceName))
                {
                    serviceList.Add(new ServiceList { ServiceName=x.ServiceName,ServiceStatus=x.Status.ToString()});
                }
            }
            WindowsServices.DataSource = serviceList;
            WindowsServices.DataBind();
        }
        protected void WindowsServices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName=="StartService")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                var serviceController = new ServiceController(((Label)WindowsServices.Rows[index].FindControl("lblServiceName")).Text.ToString());
                if (serviceController.Status != ServiceControllerStatus.Running)
                {
                    serviceController.Start();
                    serviceController.WaitForStatus(ServiceControllerStatus.Running);
                }
                PickWcisServices();
            }
            if(e.CommandName=="StopService")
            {
                int index = Convert.ToInt32(e.CommandArgument.ToString());
                var serviceController = new ServiceController(((Label)WindowsServices.Rows[index].FindControl("lblServiceName")).Text.ToString());
                if (serviceController.Status != ServiceControllerStatus.Stopped)
                {
                    serviceController.Stop();
                    serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
                }
                PickWcisServices();
            }
        }
    }
}