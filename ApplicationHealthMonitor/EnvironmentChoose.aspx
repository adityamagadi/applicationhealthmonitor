﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnvironmentChoose.aspx.cs" Inherits="ApplicationHealthMonitor.EnvironmentChoose" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Application Health Monitor</title>
    <link rel="stylesheet" href="Css/bootstrap.css" />
    <link rel="stylesheet" href="Css/CustomCss.css" />
</head>
<body>
    <div class="container">
        
        <form id="ENVChooseForm" runat="server">
            <div class="header clearfix">
                
                <h3 class="text-muted">Application Health Monitor</h3>
                
            </div>
            <div class="jumbotron">
                <div class="row">
                    <div class="col-md-4">
                        <asp:ImageButton ID="SITImage" runat="server" ImageUrl="~/Images/SIT.jpeg" ToolTip="Click to view SIT status" />
                        <br />
                        <asp:Button ID="btnSitStatus" runat="server" CssClass="btn btn-info" Text="Click to view SIT status" />
                    </div>
                    <div class="col-md-4">


                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/SIT.jpeg" ToolTip="Click to view UAT status" />
                        <asp:Button ID="btnUATStatus" runat="server" CssClass="btn btn-info" Text="Click to view UAT status" />





                    </div>
                    <div class="col-md-4">
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/SIT.jpeg" ToolTip="Click to view PROD status" />
                        <asp:Button ID="btnProdStatus" runat="server" CssClass="btn btn-info" Text="Click to view PROD status" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
