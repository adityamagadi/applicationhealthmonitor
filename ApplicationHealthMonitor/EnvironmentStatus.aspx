﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnvironmentStatus.aspx.cs" Inherits="ApplicationHealthMonitor.EnvironmentStatus" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Environment Status</title>
    <link rel="stylesheet" href="Css/bootstrap.css" />
    <link rel="stylesheet" href="Css/CustomCss.css" />
</head>
<body>
    <nav class="navbar-default">
        <div class="container-fluid">
            <div class="navbar-brand">
                <p class="navbar-brand">Application Health Monitor</p>
            </div>
        </div>
    </nav>
    <div class="jumbotron">
        <form id="FrmEnvStatus" runat="server">
            <div class="row">

                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Web UI health</div>
                        <div class="panel">

                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h6>Server Name</h6>
                                        </td>
                                        <td>
                                            <h6>IIS</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Availability</h6>
                                        </td>
                                        <td>
                                            <h6>The server is up and running</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Requests/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblRequestsPerSec" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Server Up time</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblServerUpTime" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total connection attempts/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblConnectionattepts" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total Logon attempts/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblLogonAttempts" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total application pool recycle</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblAppPoolRecycle" runat="server" Text="0"></asp:Label></h6>
                                            <br />
                                            <asp:Button ID="btnRecycleAppPool" CssClass="btn btn-default btn-danger" runat="server" Text="Click to recycle AppPool" />

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Last Poll</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblLastPoll" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Next Poll</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblNextPoll" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Windows service health</div>
                        <div class="panel">
                            <asp:GridView ID="WindowsServices" runat="server" CssClass="table" GridLines="None" DataKeyNames="ServiceName" AutoGenerateColumns="false" OnRowCommand="WindowsServices_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Service Name">
                                        <ItemTemplate>
                                            <h6>
                                                <asp:Label ID="lblServiceName" runat="server" Text='<%#Eval("ServiceName") %>'></asp:Label></h6>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <h6>
                                                <asp:Label ID="lblServiceStatus" runat="server" Text='<%#Eval("ServiceStatus") %>'></asp:Label></h6>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start/Stop">
                                        <ItemTemplate>
                                            <asp:Button ID="btnStartWinService" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="StartService" runat="server" Text="Start" CssClass="btn btn-success" />
                                            <asp:Button ID="btnStopWinService" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" CommandName="StopService" runat="server" Text="stop" CssClass="btn btn-danger" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Web service health</div>
                        <div class="panel">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <h6>Server Name</h6>
                                        </td>
                                        <td>
                                            <h6>IIS</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Availability</h6>
                                        </td>
                                        <td>
                                            <h6>The server is up and running</h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Requests/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblRequestsPerSecond_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Server Up time</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblServiceUpTime_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total connection attempts/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblTotalConnections_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total method requests/sec</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblMethodRequests_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Total application pool recycle</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblTotalAppPoolrecycle_webservice" runat="server" Text="0"></asp:Label></h6>
                                            <br />
                                            <asp:Button ID="btnRecycle_webservice" CssClass="btn btn-default btn-danger" runat="server" Text="Click to recycle AppPool" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Last Poll</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblLastPoll_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h6>Next Poll</h6>
                                        </td>
                                        <td>
                                            <h6>
                                                <asp:Label ID="lblNextPoll_webservice" runat="server" Text="0"></asp:Label></h6>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                </div>
            </div>
        </form>
    </div>
</body>
</html>
